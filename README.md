# IPSET BLOCKING MALICOUS 🛡️


## Description  📘

This playbook is designed to enhance server security by implementing IP blocking rules based on lists provided by the IP Blacklist project by Walid Ettayeb. These lists contain IP addresses associated with malicious activities, TOR nodes, and unsafe proxies. By using this playbook, administrators can harden their servers and restrict inbound and outbound traffic to and from these IPs.

## Prerequisites  📝

Before running the playbook, ensure you have filled in the hosts file:


```
[Ipset_Blocking_Malicious]
# "<TO_PROVIDE>" ansible_user="<TO_PROVIDE>" ansible_ssh_private_key_file="<TO_PROVIDE>"
```

Then execute the playbook using the following command:

```
ansible-playbook playbook_install.yml
```

By default, only inbound rules are blocked. Modify the following variables in group_vars to choose whether to block inbound, outbound, or both:

```
ipset_blocking_input: true
ipset_blocking_output: false
```

## Project Source

[IP Blacklist](https://gitlab.com/hzone/ip-blocklist/) 🚫🌐
